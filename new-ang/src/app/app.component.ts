import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

// @ts-ignore
import { GET_BOOKS } from '@dalet/root-config';

@Component({
  selector: 'new-ang-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'new-ang';
  // We use the gql tag to parse our query string into a query document
  GET_BOOKS_L = gql`
  query GetBooks {
    books {
      title
      author
    }
  }`;

  constructor(private apollo: Apollo) {
  }

  ngOnInit() {
    console.log('init1');
    console.log(this.GET_BOOKS_L === GET_BOOKS);
    this.apollo.watchQuery<any>({ query: this.GET_BOOKS_L } as any)
      .valueChanges
      .subscribe(({ data, loading }) => {
        console.log('request1-1');
        console.log(data);
      });
    this.apollo.watchQuery<any>({ query: this.GET_BOOKS_L } as any)
      .valueChanges
      .subscribe(({ data, loading }) => {
        console.log('request1-2');
        console.log(data);
      });
  }

}
