import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

// @ts-ignore
import { GET_BOOKS } from '@dalet/root-config';

@Component({
  selector: 'sec-ang-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'sec-ang';
  // We use the gql tag to parse our query string into a query document
  GET_BOOKS_L = gql`
  query GetBooks {
    books {
      title
      author
    }
  }`;

  constructor(private apollo: Apollo) {
  }

  ngOnInit() {
    console.log('init2');
    console.log(this.GET_BOOKS_L === GET_BOOKS);
    setTimeout(() => {
      this.apollo.watchQuery<any>({ query: this.GET_BOOKS_L } as any)
        .valueChanges
        .subscribe(({ data, loading }) => {
          console.log('request2-1');
          console.log(data);
        });
    }, 1000)
  }
}
