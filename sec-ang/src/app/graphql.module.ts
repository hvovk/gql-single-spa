import {APP_INITIALIZER, NgModule} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {singleSpaPropsSubject} from '../single-spa/single-spa-props';
import {take, tap} from 'rxjs/operators';
// @ts-ignore
import {apolloClient} from '@dalet/root-config';
import {of} from "rxjs";

export function initApolloClient(apollo: Apollo) {
  apollo.setClient(apolloClient);
  return () => of(apollo);
}

@NgModule({
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initApolloClient,
      deps: [Apollo],
      multi: true
    }
  ],
})
export class GraphQLModule {
}
