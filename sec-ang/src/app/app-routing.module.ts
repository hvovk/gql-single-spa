import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EmptyRouteComponent} from "./empty-route/empty-route.component";
import {BaseComponent} from "./base/base.component";
import {APP_BASE_HREF} from "@angular/common";

const routes: Routes = [
  {path: 'sec-ang', component: BaseComponent},
  {path: '**', component: EmptyRouteComponent}
];

@NgModule({
  providers: [{provide: APP_BASE_HREF, useValue: '/'}],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
