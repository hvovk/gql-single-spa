import { ParcelConfig, registerApplication, start } from 'single-spa';
import {
    constructApplications,
    constructRoutes,
    constructLayoutEngine,
} from 'single-spa-layout';
import microfrontendLayout from './microfrontend-layout.html';
import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client/core';
import gql from 'graphql-tag';

const routes = constructRoutes(microfrontendLayout);
const applications = constructApplications({
    routes,
    loadApp({ name }) {
        return System.import(name);
    },
});
const layoutEngine = constructLayoutEngine({ routes, applications });

export const apolloClient = new ApolloClient({
    uri: 'http://localhost:4000',
    cache: new InMemoryCache(),
    // credentials: 'include'
});

export const GET_BOOKS = gql`
  query GetBooks {
    books {
      title
      author
    }
  }`;

applications.forEach(registerApplication);
layoutEngine.activate();
start();
